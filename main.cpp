#include <iostream>
#include "libdxfrw.h"
#include "EntityWriter.h"
using namespace std;

int main()
    {
        dxfRW dxf("./output.dxf");
        EntityWriter writer(dxf);
        dxf.write(&writer, DRW::Version::AC1027, false);

//        QProcess::execute("librecad output.dxf");
        return 0;
    }
